import express from 'express';
import morgan from 'morgan';
import helmet from 'helmet';
import mongoose from 'mongoose';
import cors from 'cors';
import compression from 'compression';
import index from './routes/index'

class Server {
  public app : express.Application;

  constructor(){
    this.app = express();
    this.config();
    this.routes();
  }

  config(){
    
    //settings database connection
    const MONGO_URI = 'mongodb://localhost/api-node-ts';
    mongoose.set('useFindAndModify',true);
    mongoose.connect(MONGO_URI || process.env.MONGODB_URL,{
      useNewUrlParser : true,
      useUnifiedTopology: true,
      useCreateIndex:true
    })
      .then(db => console.log('DB is conected'))
      .catch(err => console.error(err));

    //settings app
    this.app.set('port',process.env.PORT || 3000);
    //middlewares
    this.app.use(express.json());
    this.app.use(express.urlencoded({extended:false}));
    this.app.use(morgan('dev'));
    this.app.use(helmet());
    this.app.use(compression());
    this.app.use(cors());
  }

  routes(){
    this.app.use(index);
  }

  start(){
    this.app.listen(this.app.get('port'),()=>{
      console.log(`server on port ${this.app.get('port')}`);
    })
  }

}

const server = new Server();
server.start();