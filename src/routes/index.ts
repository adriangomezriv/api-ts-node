import { Router } from 'express';
import { verinicio, verfinal} from '../controllers/index.controller'  

class Index {
  
  public router : Router;
  constructor(){
    this.router = Router();
    this.routes();
  }

  routes(){
    this.router.get('/',verinicio);
    this.router.get('/',verfinal);
  }
}

const index = new Index();
index.routes();

export default index.router;